base   barcode
author eIrOcA, Runout
email  office+dwbarcode@runout.at
date   2024-02-13
name   barcode -- 2D-Barcode Plugin
desc   2D-Barcode Plugin. Syntax: ~~BARCODE~id=css_id~class=css_class~mode=0~size=M~caption=x~url=https://example.com~~
url    https://gitlab.com/dokuwiki-plugins/barcode/
