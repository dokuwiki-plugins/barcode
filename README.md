# Dokuwiki Plugin: barcode

## Fork by Runout

Includes some enhancements:

* generate QR-codes locally with `qrencode` library
* generate barcodes of various types locally with `zint` library
* render the Code base64-encoded inline
* PHP 8.2 ready

## Description

Dokuwiki plugin to create 2D-Barcodes using different providers.

* qrencode (uses locally installed `qrencode` library)
* zint (uses locally installed `zint` library)
  lookup the `zint` manpage for details!
* Google Charts
* Kaywa
* I-nigma
* QRServer

Layout of the barcode can be definited by CSS styles.

## Provider qrencode

Requires the `qrencode` library. Eg for Debian:

```bash
apt install qrencode
```

## Provider zint

Requires the `zint` library. Eg for Debian:

```bash
apt install zint
```


## Links

- https://gitlab.com/dokuwiki-plugins/barcode
- [Doku](https://www.dokuwiki.org/plugin:barcode)
- [Original site](http://www.eiroca.net/doku_barcode)
