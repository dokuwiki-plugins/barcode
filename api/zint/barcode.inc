<?php
/**
 * DokuWiki Plugin barcode - GPL>=3 - See licence COPYING file
 *
 * make sure you have  installed on your linux machine
 * for Debian: apt-get install zint
 *
 * @author Markus Gschwendt (markus@runout.at)
 * @copyright Copyright (C) 2012 Markus Gschwendt
 * @license GPL >=3 (http://www.gnu.org/licenses/)
 * @version 1.0.0
 * @link http://www.runout.at
 */
class QR_zint extends QRProvider {
	// linux, zint
	function render(&$p) {
		global $conf, $ID;
		$resultStr = "--direct --filetype='PNG' ";

                if (!isset($p["data"]) && isset($p["text"])) {
                        $p["data"] = $p["text"];
                }
		if ($p["data"] == "") {
			$p["data"] = date("D M d, Y H:m:s");
		}
		$resultStr .= "--data='" . escapeshellcmd($p['data']) . "' ";
		
                if (!isset($p["scale"])) {
			if (isset($p["size"])) {
				$p["scale"] = $p["size"];
			}
			else {
                        	$p["scale"] = "M";
			}
                }

		switch($p['scale']) {
			case '':
				$resultStr .= "--scale=2 ";
			case 'S':
				$resultStr .= "--scale=1 ";
				break;
			case 'M':
				$resultStr .= "--scale=2 ";
				break;
			case 'L':
				$resultStr .= "--scale=4 ";
				break;
			case 'XL':
				$resultStr .= "--scale=8 ";
				break;
			default:
                                $size = float($p['size']);
				if ($size>0.5 and $size<100) {
					$resultStr .= "--scale=" . string($size) . " ";
				}
				else {
					$resultStr .= "--scale=2 ";
				}
				break;
		}

                // TYPE is the number or name of the  barcode  symbology.
                // To see what types are available, use the -t | --types  option.
                if (!isset($p['barcode'])) {
                        $p['barcode'] = 'qrcode';
                }

		if (!isset($p["border"])) {
			$p["border"] = "4";
		}

                if (!isset($p["vwhitesp"])) {
                        $p["vwhitesp"] = "4";
                }

                if (!isset($p["whitesp"])) {
                        $p["whitesp"] = "4";
                }

		foreach(array(i"barcode", "bg", "border", "dotsize", "fg", "height", "primary", "rotate", "secure", "vers", "vwhitesp", "whitesp") as $optional) {
			if (isset($p[$optional])) $resultStr .= "--".$optional."=".preg_replace("/[\W]/", "", $p[$optional])." ";
		}

                if (isset($p["foreground"])) {
                        $resultStr .= "--fg=".preg_replace("/[\W]/", "", $p["foreground"])." ";
                }
                if (isset($p["fgcolor"])) {
                        $resultStr .= "--fg=".preg_replace("/[\W]/", "", $p["fgcolor"])." ";
                }

                if (isset($p["background"])) {
                        $resultStr .= "--bg=".preg_replace("/[\W]/", "", $p["background"])." ";
                }
                if (isset($p["bgcolor"])) {
                        $resultStr .= "--bg=".preg_replace("/[\W]/", "", $p["bgcolor"])." ";
                }

                foreach(array("structured", "ignorecase", "8bit", "micro") as $optional) {
                        if (isset($p[$optional])) $resultStr .= "--".$optional." ";
                }

                foreach(array("bind", "bindtop", "bold", "box", "cmyk", "dotty", "embedfont", "esc", "exraesc", "fast", "nobackground", "notext", "reverse", "small", "square", "structured", "ignorecase", "8bit", "micro") as $optional) {
                        if (isset($p[$optional])) $resultStr .= "--".$optional." ";
                }

		//$filename = preg_replace("/[\W]/", "", substr(strtolower($text), 0, 250)).".png";
		//echo($resultStr);
		$uri = shell_exec("zint ".$resultStr);
		$uri = 'data:image/png;base64,' . base64_encode($uri);
		return $this->_IMG($uri, $p['id'], $p['class']);

	}
}
?>
