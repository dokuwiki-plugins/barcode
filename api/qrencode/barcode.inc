<?php
/**
 * DokuWiki Plugin barcode - GPL>=3 - See licence COPYING file
 *
 * make sure you have qrencode installed on your linux machine
 * for Debian: apt-get install qrencode
 *
 * @author Markus Gschwendt (markus@runout.at)
 * @copyright Copyright (C) 2012 Markus Gschwendt
 * @license GPL >=3 (http://www.gnu.org/licenses/)
 * @version 1.0.0
 * @link http://www.runout.at
 */
class QR_qrencode extends QRProvider {
	// linux, qrencode
	function render(&$p) {
		global $conf, $ID;
		$size = $p['size'];
		$text = escapeshellcmd($p['text']);
		if ($text == "") {
			$text = date("D M d, Y H:m:s");
		}
		switch($size) {
			case 'S':
				$resultStr = "--size=1 ";
				break;
			case 'M':
				$resultStr = "--size=2 ";
				break;
			case 'L':
				$resultStr = "--size=4 ";
				break;
			case 'XL':
				$resultStr = "--size=8 ";
				break;
			default:
				$resultStr = "--size=2 ";
				break;
		}
		foreach(array("level", "symversion", "margin", "dpi", "type") as $optional) {
			if (isset($p[$optional])) $resultStr .= "--".$optional."=".preg_replace("/[\W]/", "", $p[$optional])." ";
		}

                if (isset($p["foreground"])) {
                        $resultStr .= "--foreground=".preg_replace("/[\W]/", "", $p["foreground"])." ";
                }
                if (isset($p["fgcolor"])) {
                        $resultStr .= "--foreground=".preg_replace("/[\W]/", "", $p["fgcolor"])." ";
                }
                if (isset($p["background"])) {
                        $resultStr .= "--background=".preg_replace("/[\W]/", "", $p["background"])." ";
                }
                if (isset($p["bgcolor"])) {
                        $resultStr .= "--background=".preg_replace("/[\W]/", "", $p["bgcolor"])." ";
                }
                foreach(array("structured", "ignorecase", "8bit", "micro") as $optional) {
                        if (isset($p[$optional])) $resultStr .= "--".$optional." ";
                }

                foreach(array("structured", "ignorecase", "8bit", "micro") as $optional) {
                        if (isset($p[$optional])) $resultStr .= "--".$optional." ";
                }

		$filename = preg_replace("/[\W]/", "", substr(strtolower($text), 0, 250)).".png";
		$uri = shell_exec("qrencode ".$resultStr." -o - '".$text."'");
		$uri = 'data:image/png;base64,' . base64_encode($uri);
		return $this->_IMG($uri, $p['id'], $p['class']);

	}
}
?>
